# buildfile for the python language implementation for zamba

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-ltree-sitter -ltree-sitter-python"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/ts-python-binding.o1 ts-python-binding.scm
