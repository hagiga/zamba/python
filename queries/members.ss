(class_definition
  name: (identifier) @name) @definition.class

(function_definition
  name: (identifier) @name
  parameters: (_) @signature
  return_type: (_) @type) @definition.method

(call
  function: [(identifier) @name
             (attribute
               attribute: (identifier) @name)
             ] arguments: (argument_list) @reference.call)

(class_definition
  superclasses: (argument_list
                  (_) @name)) @reference.class
