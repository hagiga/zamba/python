;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(##namespace ("ts--python#"
parent-scopes
imported-namespaces
filter-completions
))

;; propagate the associated filetypes.
(set! languages-by-suffix
  (append languages-by-suffix
          '((".py" . "python"))))

;; add the own language-object of ts
(set! known-languages
  (cons (cons "python" (ts--python#language-new))
        known-languages))

(define parent-scopes
  '(("block" local member)
    ("class_definition" member)
    ("function_definiton" local)
    ("lambda" local)
    ("module" all)))

;; language dependend
(define keywords
  '("False"      "await"      "else"       "import"     "pass"
    "None"       "break"      "except"     "in"         "raise"
    "True"       "class"      "finally"    "is"         "return"
    "and"        "continue"   "for"        "lambda"     "try"
    "as"         "def"        "from"       "nonlocal"   "while"
    "assert"     "del"        "global"     "not"        "with"
    "async"      "elif"       "if"         "or"         "yield"))

(define (imported-namespaces uri doc)
  (let* ((ts--imports (read-file-relative-to (this-source-file) "queries/imports.ss"))
         (imports
           (fold (lambda (imp acc)
                   (let ((full (read-node (lsp-document-content doc) (cadr (member "name" imp)))))
                     (cons full acc)))
                 '("builtins" "__builtins__")
                 (ts--query-collect! doc ts--imports (ts--tree-root-node (lsp-document-tree doc))))))
    (fold (lambda (it acc) (if (member it acc) acc (cons it acc))) '() imports)))

(define (filter-completions item-vector isIncomplete-box cursor uri doc word current-node)
  ;; one could add, filter and sort items.
  ;; by providing edit-commands, one can archive import functionalities...
  ;; the cursor is already placed at the parent-scope, but the current node is provided as well.
  ;; the document is loaded and the content can be retrieved.

  ;; Note that the box can be used to tell the client that no further completions can be provided.
  ;; it comes with #t as a default.

  ;; you should return the filtered vector
  (vector-append
    ;; append the keywords.
    (list->vector
      (fold
        (lambda (keyword acc)
          (if (>= (string-length keyword) strlen)
              (if (string=? (substring keyword 0 strlen) search-string)
                  (cons `(("label" . ,keyword)
                          ("kind" . ,completion-item-kind-keyword)
                          ("detail" . "Python Syntax")) acc)
                  acc)
              acc))
        '()
        keywords))
    item-vector))
